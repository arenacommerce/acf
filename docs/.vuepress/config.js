const { fs, path } = require('@vuepress/shared-utils')
module.exports = ctx => ({
  base: '/',
  dest: 'public',
  title: 'ArenaCommerce',
  description: 'Welcome to your Knowledge Base Center',
  locales: {
    '/': {
      lang: 'en-US',
    }
  },
  markdown: {
    lineNumbers: false,
    anchor: { permalink: true },
    toc: { includeLevel: [2, 3] },
    externalLinks: {target: '_blank', rel: 'noopener noreferrer' },
    extendMarkdown: md => {
      md.set({
        html: true,
        linkify: true,
        typographer: true
         })
      md.use(require('markdown-it-multimd-table'), {enableMultilineRows: true, enableRowspan: true});
      md.use(require('markdown-it-task-lists'));
      md.use(require('markdown-it-sup'));
      md.use(require('markdown-it-sub'));
    }
  },
  head: [
    ['link', { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.6.1/css/all.css' }],
    ['link', { rel: 'icon', href: `/hero.png` }],
    ['link', { rel: 'manifest', href: '/manifest.json' }],
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
    ['meta', {name: "viewport", content: "width=device-width, initial-scale=1.0, maximum-scale=1.0"}],
    ['link', { rel: 'apple-touch-icon', href: `/icons/apple-touch-icon-152x152.png` }],
    ['link', { rel: 'mask-icon', href: '/icons/safari-pinned-tab.svg', color: '#3eaf7c' }],
    ['meta', { name: 'msapplication-TileImage', content: '/icons/msapplication-icon-144x144.png' }],
    ['meta', { name: 'msapplication-TileColor', content: '#000000' }]
  ],
  theme: '@vuepress/default',
  themeConfig: {
    logo: '/logo_web.png',
    search: true,
    algolia: {
    apiKey: '066a0082a477b7638ecca8d4960343a0',
    indexName: 'advancedcustomfield_help'
    },
    editLinks: false,
    activeHeaderLinks : true,
    locales: {
      '/': {
        label: 'English',
        selectText: 'Languages',
        nav: require('./nav/en'),
        sidebar: {
          '/acf/': [
            '',
            'general',
            'fields-tab', 
            'editor-tab',
            'field-editor',
            'group-editor',
            'data-types',
            'reference-types',
            'unrepeatable-repeatable',
            'search',
          ],
          '/product-page/' :[
            '',
          ]
        }
      },
    }
  },
  plugins: [
    ['@vuepress/search', {
      searchMaxSuggestions: 10,
    }],
    ['tabs'],
    ['flowchart'],
    ['@vuepress/medium-zoom', {
      selector: '.theme-default-content:not(.custom) img',
      options: {
        margin: 16,
        background: '#BADA55',
      }
    }],
    ['@vuepress/back-to-top', true],
    ['@vuepress/pwa', {
      serviceWorker: true,
      updatePopup: true
    }],
    ['@vuepress/google-analytics', {
      ga: 'UA-153551292-1'
    }],
    ['container', {
      type: 'vue',
      before: '<pre class="vue-container"><code>',
      after: '</code></pre>',
    }],
    ['container', {
      type: 'upgrade',
      before: info => `<UpgradePath title="${info}">`,
      after: '</UpgradePath>',
    }],
    ['container', {
      type: 'right',
      defaultTitle: '',
    }],
    ['container', {
      type: 'theo',
      before: info => `<div class="theo"><p class="title">${info}</p>`,
      after: '</div>',
    }],
    ['vuepress-plugin-code-copy', {
      color: '#1c6ab6'
    }]
  ],
  extraWatchFiles: [
    '.vuepress/nav/en.js'
  ]
})