module.exports = [
  {
    text: 'Terms Of Service',
    link: '/tos/'
  },
  {
    text: 'Advanced Custom Field',
    link: '/acf/'
  },
  {
    text: 'Usage Examples',
    // link: '/usage-examples/'
    items: [
      {
        text: 'Article Page',
        link: '/article-page/'
      },
      {
        text: 'Cart Page',
        link: '/cart-page/'
      },
      {
        text: 'Collection Page',
        link: '/collection-page/'
      },
      {
        text: 'Order Status Page',
        link: '/order-status-page/'
      },
      {
        text: 'Product Page',
        link: '/product-page/'
      },
      {
        text: 'Hide a resource from search engines and sitemaps',
        link: '/others/'
      },
    ]
  },
  {
    text: 'Create Support Ticket',
    link: 'https://arenacommerce.freshdesk.com/support/tickets/new'
  },
]
