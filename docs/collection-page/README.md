---
metaTitle: Adding Custom Fields Data (Metafield Data) to the Collection Page
sidebarDepth: 2
---

This is advanced guide, so please make sure you have understand our app well before going forward. If you are still not sure or have not gone through ACF's basic knowledge, we recommend you go to [Advanced Custom Field](/acf/).

Our examples were created on Shopify's [Debut theme](https://themes.shopify.com/themes/debut/styles/default). This example requires to edit theme code, so users need to have some coding knowledge. Different theme may also has different source code but overall the theme structure should be the same since they are all Shopify themes.

In case you have problem applying any examples here to your theme, please submit a support request via our [Support Portal](https://arenacommerce.freshdesk.com/support/tickets/new); or send email to support@arenacommerce.com.

---

You have a sale collection and you wonder how to add sale expired date on that collection to encourage customers to buy? This example will show you how to achive it.

- <u>**Step 1**</u>: At Fields tab, choose resource **Collections** and create a [Date field](/acf/data-types.html#_9-date).
![](/acf/sale-expired-date-1.png) 
- <u>**Step 2**</u>: Click **Save** button to save your changes; then go to Editor tab's resource **Collections** and pick a collection to start with.
- <u>**Step 3**</u>: Input a date and click **Save** button to save your changes.
![](/acf/sale-expired-date-2.png) 
- <u>**Step 4**</u>: Go to [Edit code](https://help.shopify.com/en/manual/using-themes/change-the-layout/theme-code), open file **collection-template.liquid**. In that file, search keyword **filters-toolbar-wrapper**, then add the following code:

``` liquid
{% if collection.metafields.namespace.key != blank %}
  <div class="sale-expired-date page-width" style="text-align: center; font-size: 50px;">Hurry up! Sale will end on {{collection.metafields.namespace.key}}!!</div>
{% endif %}
```

![](/acf/sale-expired-date-3.png) 
- <u>**Step 5**</u>: Click **Save** button to save your changes.

If the above steps are done correctly, the [result](https://i.arenacommerce.com/go/acf/advancedcustomfield-collections-all-in-one/) should be displayed on corresponding collection page.
![](/acf/sale-expired-date-4.png)