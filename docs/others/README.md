---
metaTitle: Hide a resource from search engines and sitemaps
sidebarDepth: 2
---

# Hide a resource from search engines and sitemaps

This advanced guide is based on Shopify's official [documentation](https://help.shopify.com/en/api/guides/updating-seo-data#hide-a-resource-from-search-engines-and-sitemaps) and can be applied to other resources like *Products*, *Product Variants*, *Collections*, *Blogs*, *Posts* and *Pages*. Here in our examples we will apply it to resource **Pages**.

Please also note that since this is advanced guide, make sure you have understand our app well before going forward. If you are still not sure or have not gone through ACF's basic knowledge, we recommend you go to [Advanced Custom Field](/acf/).

In case you have problem applying any examples here to your theme, please submit a support request via our [Support Portal](https://arenacommerce.freshdesk.com/support/tickets/new); or send email to support@arenacommerce.com.

---

- <u>**Step 1**</u>: At Fields tab, choose resource **Pages** and create a [Switch field](/acf/data-types.html#_6-switch).
![](/acf/hide-seo-1.png) 
- <u>**Step 2**</u>: Click **Save** button to save your changes; then go to Editor tab's resource **Pages** and pick a page to start with.
- <u>**Step 3**</u>: Switch the button on and click **Save** to save your changes.
![](/acf/hide-seo-2.png) 

And... That's it! You have successfully hide a resource from search engines and sitemaps using metafield.

