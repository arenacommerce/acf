---
metaTitle: Usage Examples
sidebarDepth: 2
---

# Usage Examples

This is advanced guide, so please make sure you have understand our app well before going forward. If you are still not sure or have not gone through ACF's basic knowledge, we recommend you go to [Advanced Custom Field](/acf/).

Our examples were created on Shopify's [Debut theme](https://themes.shopify.com/themes/debut/styles/default). These examples require to edit theme code, so users need to have some coding knowledge. Different theme may also has different source code but overall the theme structure should be the same since they are all Shopify themes.

In case you have problem applying any examples here to your theme, please submit a support request via our [Support Portal](https://arenacommerce.freshdesk.com/support/tickets/new); or send email to support@arenacommerce.com.

<!-- For customers who are also using our [Shopify themes](https://themeforest.net/user/arenatheme/portfolio), the followings are examples of metafields supported by our themes. -->

<!-- ## Examples of metafields supported in ArenaCommerce's themes

#### <u>Note</u>: 
- All of the following metafields are for resource **Products**.
- To know which metafields in the list below can be used in your ArenaCommerce theme(s), please refer to corresponding theme's documentation or [contact us](https://arenacommerce.freshdesk.com/support/home).  

### Bundle Product 
- <u>**Step 1**</u>: At Field tab, [create a group](/acf/fields-tab.html#add-new-group) called **Bundle Product**.
- <u>**Step 2**</u>: Create 2 [product reference fields](/acf/types-of-fields.html#_13-product) in that group

	```
	- namespace: c_f
  	- key: bundle_1
  	- label: Add your product handle #1!
  	```

  	```
  	- namespace: c_f
  	- key: bundle_2
  	- label: Add your product handle #2!
  	```

> ##### <u>Tip #1</u>: How to [add new field](/acf/fields-tab.html#add-new-field).  	

- <u>**Step 3**</u>: Click **Save** button to save your changes; then go to Editor tab's resource "*Products*" and pick a product to start with.

> ##### <u>Tip #2</u>: Use search bar to quickly [find specific products](/acf/search.html#products).

- <u>**Step 4**</u>: On next page, pick a product item for each field editor via resource picker.
![](/acf/bundle-product.png) 

> ##### <u>Tip #3</u>: Use left - right arrow buttons to navigate to previous - next items.

- <u>**Step 5**</u>: Click **Save** button to save your changes. 

And... That's it! **Bundle Product** is all set up. You can refer to our example work at https://electro.arenacommerce.com/products/gold-diamond-chain
![](/acf/bundle-product-sample.png) 


### Exit Intent Popup Product 
- <u>**Step 1**</u>: At Field tab, [create a group](/acf/fields-tab.html#add-new-group) called **Exit Intent Popup Product**.
- <u>**Step 2**</u>: Create 2 [text fields](/acf/types-of-fields.html#_1-text) in that group
	
	```
	- namespace: c_f
  	- key: discount_code
  	- label: Add your Coupon code for the product
  	```

  	```
  	- namespace: c_f
  	- key: discount_percent
  	- label: Display percent of discount
  	```

- <u>**Step 3**</u>: Click **Save** button to save your changes; then go to Editor tab's resource "*Products*" and pick a product to start with.
- <u>**Step 4**</u>: On next page, add your coupon code and display percent of discount
![](/acf/exit-intent-discount-popup.png) 
- <u>**Step 5**</u>: Click **Save** button to save your changes. 

**Exit Intent Popup Product** is all set up. You can refer to our example work at https://electro.arenacommerce.com/products/black-fashion-zapda-shoes
![](/acf/exit-intent-discount-popup-sample.png) 


### Countdount Timer
- <u>**Step 1**</u>: At Field tab, create a [date field](/acf/types-of-fields.html#_7-date)

	```
	- namespace: c_f
  	- key: countdown_timer
  	- label: Date format is mm/dd/yyyy
  	```
It is required to set **date format** as *mm/dd/yyyy* in order for countdown to work.

- <u>**Step 2**</u>: Click **Save** button to save your changes; then go to Editor tab's resource "*Products*" and pick a product to start with.
- <u>**Step 3**</u>: On next page, pick a date 
![](/acf/countdown-timer.png) 
- <u>**Step 4**</u>: Click **Save** button to save your changes.

**Countdount Timer** is all set up. You can refer to our example work at https://electro.arenacommerce.com/products/black-fashion-zapda-shoes
![](/acf/countdown-timer-2.png) 


### Short Description
- <u>**Step 1**</u>: At Field tab, create a [HTML field](/acf/types-of-fields.html#_2-html)

	```
	- namespace: c_f
  	- key: description_excerpt
  	- label: Add the product short description!
  	```

- <u>**Step 2**</u>: Click **Save** button to save your changes; then go to Editor tab's resource "*Products*" and pick a product to start with.
- <u>**Step 3**</u>: On next page, add product short description
![](/acf/short-description.png) 
- <u>**Step 4**</u>: Click **Save** button to save your changes.

**Short Description** is all set up. You can refer to our example work at https://electro.arenacommerce.com/products/black-fashion-zapda-shoes
![](/acf/short-description-2.png)  -->
 

<!-- ## :black_medium_small_square: Adding Custom Fields Data (Metafield Data) to the Order Status Page
Custom Field (metafield) data about an order can be added to the Order Status page that customers see when they check the status of the order. (Normally when customers submit an order, an email will be sent to them with a link from which they can view their order status). 

In order to make this work, there needs to be at least one order on your store.

In this example, we will show you how to achive this:
- <u>**Step 1**</u>: At Fields tab, choose resource **Orders** and create a type of fields you want (Here in our example we chose [Text field](/acf/types-of-fields.html#_1-text)).
![](/acf/order-status-1.png) 

- <u>**Step 2**</u>: Click **Save** button to save your changes; then go to Editor tab's resource **Orders** and pick an order to start with.
- <u>**Step 3**</u>: On next page, add custom field data. 
![](/acf/order-status-2.png)
- <u>**Step 4**</u>: Click **Save** button to save your changes.
- <u>**Step 5**</u>: In order to make this work, an additional script needs to be added to **/admin/Settings/Checkout**

This example script adds data from a metafield that is attached to the order itself. 

``` liquid
<script>
  Shopify.Checkout.OrderStatus.addContentBox(
    '<h2>My new heading</h2><p>{{ order.metafields.namespace.key }}</p>',
  );
</script>
```
![](/acf/order-status-3.png)

If the above steps are done correctly, the result should be displayed on Order Status page
![](/acf/order-status-4.png) -->


<!-- ## :black_medium_small_square: Displaying Custom Fields as Bullet Points on Product Pages
In this advanced guide, we'll show you how to use fields in ACF to create a bullet list displayed to customers on products. This is another example to show you how you can utilize ACF.

- <u>**Step 1**</u>: At Fields tab, choose resource **Products** and create a [HTML field](/acf/types-of-fields.html#_2-html).
![](/acf/bullet_points_1.png) 

- <u>**Step 2**</u>: Click **Save** button to save your changes; then go to Editor tab's resource **Products** and pick a product to start with.
- <u>**Step 3**</u>: On next page, click **Unordered list** and add content you want.
![](/acf/bullet_points_2.png) 
- <u>**Step 4**</u>: Click **Save** button to save your changes.
- <u>**Step 5**</u>: Go to [Edit code](https://help.shopify.com/en/manual/using-themes/change-the-layout/theme-code), open file **product-template.liquid**. In that file, search keyword **product__price**, then add the following code:

``` liquid
{% if product.metafields.namespace.key != blank %}
  <div class="metafield-bullet-points">{{product.metafields.namespace.key}}</div>
  <style>
    .metafield-bullet-points {
        margin-top: 1rem;
        margin-left: 20px;
        margin-right: 20px;
    }
    .metafield-bullet-points ul li {list-style: initial;}
  </style>
{% endif %}
```
![](/acf/bullet_points_3.png) 
- <u>**Step 6**</u>: Click **Save** button to save your changes.

If the above steps are done correctly, the [result](https://i.arenacommerce.com/go/acf/advancedcustomfield-consectetur-nibh-eget/) should be displayed on corresponding product page.
![](/acf/bullet_points_4.png)

::: tip
Click **Preview** button to quickly jump to corresponding product page.
![](/acf/preview_button.png) 
::: -->


<!-- ## :black_medium_small_square: Creating a Simple Shipping Policy on Product Pages
In this advanced tutorial, we will guide you through how to create a product field to display as a per-product shipping policy for your customers. This can be useful if you have different shipping times for different types of products.

In this example, we will show you how to achive this:
- <u>**Step 1**</u>: At Fields tab, choose resource **Products** and create a [Selection field](/acf/types-of-fields.html#_3-selection).
![](/acf/shipping-policy-1.png)
- <u>**Step 2**</u>: Click **Save** button to save your changes; then go to Editor tab's resource **Products** and pick a product to start with.
- <u>**Step 3**</u>: Pick an shipping policy option for your product variant. When everything is fine, click **Save** button to save your changes.
![](/acf/shipping-policy-2.png)
- <u>**Step 4**</u>: Go to [Edit code](https://help.shopify.com/en/manual/using-themes/change-the-layout/theme-code), open file **product-template.liquid**. In that file, search keyword **product-single__description**, then add the following code:

``` liquid
{% if product.metafields.namespace.key != blank %}
  <div class="shipping-policy">
    <h2>Shipping Policy</h2>
    <p>This item takes {{product.metafields.namespace.key}} to ship.</p>
  </div>
{% endif %}
```
![](/acf/shipping-policy-3.png)
- <u>**Step 5**</u>: Click **Save** button to save your changes.

If the above steps are done correctly, the [result](https://i.arenacommerce.com/go/acf/advancedcustomfield-black-fashion-zapda-shoes/) should be displayed on corresponding product page.
![](/acf/shipping-policy-4.png) -->


<!-- ## :black_medium_small_square: Displaying Text Based on a Checkbox Value on Product Pages
Theme designers can completely control the output and placement of any custom field in the app using some advanced techniques.

Some possible uses include creating a banner image on every product page, creating a header message area, showing different icons based on product attributes, and many other custom uses.

In this example, we'll add a checkbox to our products that will show one message if checked and another if it's not checked, but you can adapt this example for many other uses.
- <u>**Step 1**</u>: At Fields tab, choose resource **Products** and create a [Checkbox field](/acf/types-of-fields.html#_5-checkbox).
![](/acf/checkbox-1.png)
- <u>**Step 2**</u>: Click **Save** button to save your changes; then go to Editor tab's resource **Products** and pick a product to start with.
- <u>**Step 3**</u>: Check the **Recyclable?**.
![](/acf/checkbox-2.png)
- <u>**Step 4**</u>: Go to [Edit code](https://help.shopify.com/en/manual/using-themes/change-the-layout/theme-code), open file **product-template.liquid**. In that file, search keyword **product__price**, then add the following code:

``` liquid
{% if product.metafields.namespace.key %}
  <p>This product is recyclable</p>
  {% else %}
  <p>This product is not recyclable</p>
{% endif %}
```
![](/acf/checkbox-3.png)
- <u>**Step 5**</u>: Click **Save** button to save your changes.

If the above steps are done correctly, the [result](https://i.arenacommerce.com/go/acf/advancedcustomfield-coneco-product-sample/) should be displayed on corresponding product page.
![](/acf/checkbox-4.png) -->


<!-- ## :black_medium_small_square: Displaying Total Views on Article Pages
In this example, we will show you how to add custom field data to articles to pretend their total views.
- <u>**Step 1**</u>: At Fields tab, choose resource **Posts** and create a [Number field](/acf/types-of-fields.html#_7-number).
![](/acf/post-view-1.png) 
- <u>**Step 2**</u>: Click **Save** button to save your changes; then go to Editor tab's resource **Posts** and pick a post to start with.
![](/acf/post-view-2.gif) 
- <u>**Step 3**</u>: Input a number and click **Save** button to save your changes.
![](/acf/post-view-3.png) 
- <u>**Step 4**</u>: Go to [Edit code](https://help.shopify.com/en/manual/using-themes/change-the-layout/theme-code), open file **article-template.liquid**. In that file, search keyword **rte**, then add the following code:

``` liquid
{% if article.metafields.namespace.key != blank %}
  <div class="total-view"><h5>Total view(s): <span style="font-weight: 100;">{{article.metafields.namespace.key}}</span></h5></div>
{% endif %}
```
![](/acf/post-view-4.png) 
- <u>**Step 5**</u>: Click **Save** button to save your changes.

If the above steps are done correctly, the [result](https://i.arenacommerce.com/go/acf/advancedcustomfield-blogs-news-1_sweet-memorires-in-our-store/) should be displayed on corresponding article page.
![](/acf/post-view-5.png)  -->


<!-- ## :black_medium_small_square: Displaying Sale Expired Date on Collection Pages
You have a sale collection and you wonder how to add sale expired date on that collection to encourage customers to buy? This example will show you how to achive it.

- <u>**Step 1**</u>: At Fields tab, choose resource **Collections** and create a [Date field](/acf/types-of-fields.html#_9-date).
![](/acf/sale-expired-date-1.png) 
- <u>**Step 2**</u>: Click **Save** button to save your changes; then go to Editor tab's resource **Collections** and pick a collection to start with.
- <u>**Step 3**</u>: Input a date and click **Save** button to save your changes.
![](/acf/sale-expired-date-2.png) 
- <u>**Step 4**</u>: Go to [Edit code](https://help.shopify.com/en/manual/using-themes/change-the-layout/theme-code), open file **collection-template.liquid**. In that file, search keyword **filters-toolbar-wrapper**, then add the following code:

``` liquid
{% if collection.metafields.namespace.key != blank %}
  <div class="sale-expired-date page-width" style="text-align: center; font-size: 50px;">Hurry up! Sale will end on {{collection.metafields.namespace.key}}!!</div>
{% endif %}
```

![](/acf/sale-expired-date-3.png) 
- <u>**Step 5**</u>: Click **Save** button to save your changes.

If the above steps are done correctly, the [result](https://i.arenacommerce.com/go/acf/advancedcustomfield-collections-all-in-one/) should be displayed on corresponding collection page.
![](/acf/sale-expired-date-4.png) -->


<!-- ## :black_medium_small_square: Displaying Product Fields on the Cart Page
You can easily show field data from products in your cart. This is useful if you want to show information at the last minute about that product. In this example, let say you offer gift wrapping service for your product and you want to show wrapping paper's color as product's additional information. So we will show you how to print information about wrapping paper's color on Cart Page.

- <u>**Step 1**</u>: At Fields tab, choose resource **Products** and create a [Color field](/acf/types-of-fields.html#_10-color).
![](/acf/color-1.png)
- <u>**Step 2**</u>: Click **Save** button to save your changes; then go to Editor tab's resource **Products** and pick a product to start with.
- <u>**Step 3**</u>: Pick a color and click **Save** button to save your changes.
- <u>**Step 4**</u>: Go to [Edit code](https://help.shopify.com/en/manual/using-themes/change-the-layout/theme-code), open file **cart-template.liquid**. In that file, search keyword `</ul>`, then add the following code:

``` liquid
{%- if item.product.metafields.namespace.key != blank -%}
  <li class="product-details__item product-details__item--wp-color">Wrapping paper's color: <span title="{{ item.product.metafields.namespace.key }}" style="height: 20px; width: 20px; display: inline-flex; vertical-align: bottom; background-color:{{ item.product.metafields.namespace.key }}"></span></li>
{%- endif -%}
```

![](/acf/color-2.png) 
- <u>**Step 5**</u>: Click **Save** button to save your changes.

If the above steps are done correctly, the result should be displayed on cart page.
![](/acf/color-3.png)  -->



















