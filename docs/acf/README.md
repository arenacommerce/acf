---
metaTitle: Overview Advanced Custom Field- ArenaCommerce,
sidebarDepth: 2
---


# Overview

You can use metafields to add custom fields to objects such as products, customers, and orders. Metafields are useful for storing specialized information, such as part numbers, customer titles, or blog post summaries. They can be used by apps and channels to track data for internal use.

**Advanced Custom Field (ACF)** a powerful add-on application for your Shopify store that utilizes Shopify's Metafields. It enables admins to add **structured metafield fields** (custom fields) to things in Shopify (objects), such as products, collections, orders, customers, and pages. These extra content fields are more commonly referred to as Custom Fields and can allow you to build websites faster and educate your clients quicker.

![](/acf/customfield.gif) 

ACF supports adding custom fields to 9 Shopify object:

- [Product](https://help.shopify.com/en/themes/liquid/objects/product)
- [Product Variants](https://help.shopify.com/en/themes/liquid/objects/variant)
- [Collections](https://help.shopify.com/en/themes/liquid/objects/collection)
- [Pages](https://help.shopify.com/en/themes/liquid/objects/page)
- [Blogs](https://help.shopify.com/en/themes/liquid/objects/blog)
- [Posts](https://help.shopify.com/en/themes/liquid/objects/article)
- [Customers](https://help.shopify.com/en/themes/liquid/objects/customer)
- [Orders](https://help.shopify.com/en/themes/liquid/objects/order)
- [Shop](https://help.shopify.com/en/themes/liquid/objects/shop) (aka Global metafields).


## The basics

In this guide, you’ll learn how to:

1. Create new custom field- **Fields**
2. Create custom field content - Add metafields to object as field settings. - **Editor**
3. Show custom field in the storefront


### 1. Creating Fields

![](/overview.png)

Creating new custom fields is a very easy process and can be done with just a few clicks of our user friendly field builder! You can create as many fields as you like, each with their own name, type and settings. 

Each field is added to a group which allows you to both organize your fields and specify the edit screens where they appear.

**Field groups** are used to organize fields and attach them to edit screens. Each field group contains **a title, fields, location rules and visual settings** (colors,..).

To get started with your first field group, please read our [Creating a Field Group guide](/acf/group-editor.html).


**Each field contains settings to customize how the field looks (its type)**, where its value is saved (its name) and how it functions (its settings). These field settings can be customized when editing a field group.

To learn more about field settings, please read our Field Settings guide.


### 2. Editor - Content Data

With your fields created, it’s time to start editing your content! All our fields are very intuitive to use and display seamlessly with the Shopify admin style.
![](/acf/editor_overview.gif)

To get an idea of the different content you can edit in ACF, please look over the available [data types](/acf/data-types.html) and [reference types](/acf/reference-types.html).

<u>**How to access ACF**</u>

After successfully installing ACF, every time you need to use the app you can access it via 2 ways:

1. From your Shopify admin, go to **Apps**. Click **Advanced Custom Field**.
![](/acf/access-acf.png) 

2. Using ACF's [admin link](https://help.shopify.com/en/api/embedded-apps/app-extensions/shopify-admin/app-admin-access) in the **More actions** menu.
![](/acf/access-acf-2.png) 


### 3. Displaying Fields

To show custom field details in your storefront, use the metafields object in your Liquid theme pages (including checkout.liquid) and in other locations where Liquid variables are supported.

For example, you could use the following Liquid code to display the example washing instructions on a product page:

``` liquid
{% unless product.metafields.Acme134-instructions.Wash == blank %}
	Wash: {{ product.metafields.Acme134-instructions.Wash }}
{% endunless %}
```

## Definition of special terms used in ACF app and this document
- <u>**Resource**</u>: resource indicates types of subjects ACF supports, which are: products, product variants, collections, blogs, posts, pages, customers, orders, shop.
![](/acf/resource.png) 

- <u>**Resource list**</u>: resource list is an item list of a specific resource. For example if current selected resource is products, its corresponding resource list will be a list of products.
![](/acf/resource-list.png)

	**Collections** and **orders** are 2 special resources because they each have 2 types of resource list:
	- Collections:
	
	:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
	::: tab "Collections"
	![](/acf/collections-collections.png) 
	:::
	::: tab "Smart Collections"
	![](/acf/collections-smart.png) 
	:::
	::::

    - Orders: 
	:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
	::: tab "Orders"
	![](/acf/orders.png) 
	:::
	::: tab "Draft Orders"
	![](/acf/draft-orders.png) 
	:::
	::::

- **[Group](/acf/fields-tab.html#add-new-group)**: group is used to contain a block of fields.
- **[Field](/acf/fields-tab.html#add-new-field)**: field represents type of field editor and can be placed in or outside a parent group. When field is inside a parent group, field's 2 attributes **Repeatable** and **Field applies to** will be dependent on the parent group's.
- **[Unrepeatable (Non-repeatable) - Repeatable](/acf/unrepeatable-repeatable.html)**
- **[Bulk editor](https://help.shopify.com/en/manual/productivity-tools/bulk-editing-products)**: according to Shopify document: *"with the bulk editor, which is similar to a spreadsheet, you can edit multiple products and their variants at once from your Shopify admin"*. In term of metafield, it can be used to edit product, product variant, collection, page or post (article) metafields. 
- **[Field editor](/acf/field-editor.html)**: it is used to input content to a metafield.
- <u>**JSON files**</u>: they are created by ACF when you export data.
- **[Search bar](/acf/search.html)**: it is used to search for items in a **resource list**. 

::: tip
Use the Search tool in top to quickly find all the keywords which you are looking for.
:::

## Where are my custom fields stored?
All custom field definitions as well as the values you enter for individual pages, products etc. are stored as metafields within your Shopify store. Metafields are an integral part of the Shopify platform and besides providing secure storage for your data, they can immediately be used in your design templates to show your new data to your visitors.
