---
metaTitle: ACF - Data Types
---

# ACF - Data Types

There are 13 data types in ACF. All value produced by ACF is either in string/integer (non-repeatable field) or JSON string (repeatable field).

![](/acf/data-types.png)

## 1. Text
- **Text** field helps users to add text content.
- **Text** field can be adjusted up to 10 lines.
- **Text** field can be edited in Editor tab via text field editor.

:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Text field settings"
![](/acf/text-field-settings.png)  
:::
::: tab "Text field editor"
![](/acf/text-field-editor.png) 
:::
::::

- Template usage example:
```liquid
<h2>My new heading</h2><p>{{ order.metafields.namespace.key }}</p>
```
- [Usage example](/order-status-page/)

## 2. HTML
- **HTML** field is like text field but more advanced. Users can add not only text but also HTML content.
- **HTML** field can be edited in Editor tab via text field editor.
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "HTML field settings"
![](/acf/html-field-settings.png) 
:::
::: tab "Text input form"
![](/acf/html-field-editor.png)  
:::
::: tab "HTML input form (Code view)"
![](/acf/html-field-editor-2.png)
:::
::::
<!-- ::: danger WARNING
Due to the limitation of this HTML plugin, content is not read in **HTML input form**. Therefore after done editing in **HTML input form**, the editor **MUST BE** switched back to **Text input form** in order for the plugin to update content!
::: -->

- Template usage example:
``` liquid
<div class="metafield-bullet-points">{{product.metafields.namespace.key}}</div>
```
- [Usage example](/product-page/#displaying-custom-fields-as-bullet-points-on-product-pages)

## 3. Selection
- **Selection** field lets users select one or multiple pre-defined value.
- When attribute "**Allow selection of multiple values**" is checked, each value will be separated by **|**, for example: hello<b>|</b>world<b>|</b>2020, book<b>|</b>shoes<b>|</b>pen<b>|</b>eraser.
- **Selection** field can be edited in Editor tab via selection field editor. 
	- Single-value selection:
	:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
	::: tab "Selection field settings"
	![](/acf/selection-field-settings.png) 
	:::
	::: tab "Selection field editor"
	![](/acf/selection-field-editor.png)
	:::
	::::

	- Multiple-value selection:
	:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
	::: tab "Selection field settings"
	![](/acf/selection-field-settings-2.png) 
	:::
	::: tab "Selection field editor"
	![](/acf/selection-field-editor-2.png)
	:::
	::::

- Template usage example:
``` liquid
<p>Included item is {{product.metafields.namespace.key}}.</p>
```
- [Usage example](/product-page/#creating-a-simple-shipping-policy-on-product-pages)

## 4. Choice
- **Choice** field lets user select one pre-defined value.
- When attribute "**Use icon?**" is checked, inputted value must be in format `Choice label::: icon's class name` in order to display icons. More info please go to [Fields tab](/acf/fields-tab.html#add-new-field), article **3. Choice field**.
- **Choice** field can be edited in Editor tab via choice field editor
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Choice field settings"
![](/acf/choice-field-settings.png)
:::
::: tab "Choice field editor"
![](/acf/choice-field-editor.png)
:::
::::

## 5. Checkbox
- **Checkbox** field is used as a way to have users indicate they agree to specific terms, services or to make simple selection like true-false, yes-no etc. Value created by **Checkbox** field is **boolean (true/false)**.
- **Checkbox** field can be edited in Editor tab via checkbox field editor.
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Checkbox field settings"
![](/acf/checkbox-field-settings.png)
:::
::: tab "Checkbox field editor"
![](/acf/checkbox-field-editor.png)
:::
::::

- Template usage example:
``` liquid
{% if product.metafields.namespace.key %}
  <p>This product is recyclable</p>
  {% else %}
  <p>This product is not recyclable</p>
{% endif %}
```
- [Usage example](/product-page/#displaying-text-based-on-a-checkbox-value-on-product-pages)

## 6. Switch
- **Switch** field lets user switch on or off an option. It can also be used to make simple selection like true-false, yes-no. Value created by **Switch** field is **integer (1/0)**.
- **Switch** field can be edited in Editor tab via checkbox field editor.
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Switch field settings"
![](/acf/switch-field-settings.png)
:::
::: tab "Switch field editor"
![](/acf/switch-field-editor.gif)
:::
::::
- [Usage example](/others/)

## 7. Number
- **Number** field helps users to add number only.
- Depend on pre-defined number's value type in settings, value created by **Number** can be either *string* or *integer*. However, if field is set to be repeatable, value type will be converted to *JSON string*.
- **Number** field can be edited in Editor tab via number field editor.
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Number field settings"
![](/acf/number-field-settings.png)
:::
::: tab "Number field editor"
![](/acf/number-field-editor.png)
:::
::::
- If **Number**'s value type is set as *integer*, the number field editor will not accept decimal and will pop up an error if decimal is inputted:
![](/acf/decimal-error.png)
- Template usage example:
``` liquid
<div class="total-view"><h5>Total view(s): <span style="font-weight: 100;">{{article.metafields.namespace.key}}</span></h5></div>
```
- [Usage example](/article-page/)

## 8. Range
- **Range** field allows users to select a numeric value within a given range (minimum and maximum values).
- **Range** field can be edited in Editor tab via range slider. 
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Range field settings"
![](/acf/range-field-settings.png)
:::
::: tab "Range field editor"
![](/acf/range-field-editor.png)
:::
::::

## 9. Date
- **Date** field allows users to set date for specific events, occasions, activities etc. based on pre-defined date format.
- **Date** field can be edited in Editor tab via text field editor or date picker. Date picker is synced with the text input field, so users only need to use one of them to add a date.
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Date field settings"
![](/acf/date-field-settings.png)
:::
::: tab "Date field editor"
![](/acf/date-field-editor.png)
:::
::::
- Template usage example:
``` liquid
<div class="sale-expired-date page-width" style="text-align: center; font-size: 50px;">Hurry up! Sale will end on {{collection.metafields.namespace.key}}!!</div>
{% endif %}
```
- [Usage example](/collection-page/)

## 10. Color
- **Color** field allows users to add [HTML color names](https://www.w3schools.com/colors/colors_names.asp), color hex codes, color rgb codes or color rgba codes.
- **Color** field can be edited in Editor tab via text input field or color picker. Color picker is synced with the text input field, so users only need to use one of them to add a color.
![](/acf/color-field-settings.png) 
![](/acf/color-field-editor.png)
![](/acf/color-field-editor-2.gif) 	
- Template usage example:
``` liquid
{%- if item.product.metafields.namespace.key != blank -%}
  <li class="product-details__item product-details__item--wp-color">Wrapping paper's color: <span title="{{ item.product.metafields.namespace.key }}" style="height: 20px; width: 20px; display: inline-flex; vertical-align: bottom; background-color:{{ item.product.metafields.namespace.key }}"></span></li>
{%- endif -%}
```
- [Usage example](/cart-page/)

## 11. Image/File
- **Image/File** field allows users to add image URL.
- **Image/File** field can be edited in Editor tab via text input field. 
![](/acf/image-field-settings.png)

<!-- - Single-value inputter:
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Image/File field settings"
![](/acf/image-field-settings.png)
:::
::: tab "Image/File field editor"
![](/acf/image-field-editor.png)
:::
::::

- Multiple-value inputter:
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Image/File field settings"
![](/acf/image-field-settings-2.png)
:::
::: tab "Image/File field editor"
![](/acf/image-field-editor-2.png)
:::
:::: -->
- Template usage example:
``` liquid
{%- unless product.metafields.namespace.key == blank -%}
  	<div class="custom-field--content product-custom-field--image-size-chart">
  		<img src="{{product.metafields.namespace.key}}" alt="size_chart_customfield">
  	</div
{%- endunless -%}
```
<!-- - [Usage example](/product-page/#displaying-an-image-size-chart-on-product-pages) -->

## 12. Time
- **Time** field allows users to set time for specific events, occasions, activities etc.
- **Time** field can be edited in Editor tab via time picker.
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Time field settings"
![](/acf/time-field-settings.png)
:::
::: tab "Time field editor"
![](/acf/time-field-editor.png)
:::
::::

## 13. JSON 
- **JSON** field allows users to add JSON content.
- **JSON** field can be edited in Editor tab via JSON editor.
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "JSON field settings"
![](/acf/json-field-settings.png)
:::
::: tab "JSON field editor"
![](/acf/json-field-editor.png)
:::
::::

- Since it is a JSON editor, it can check JSON error while typing.
![](/acf/json-field-editor-2.png)

## 14. Tags
- **Tags** field allows users to add a list of tags. 
- When attribute "**Allow selection of multiple values**" is checked, each value will be separated by **|**, for example: hello<b>|</b>world<b>|</b>2020, book<b>|</b>shoes<b>|</b>pen<b>|</b>eraser.
- **Tags** field can be edited in Editor tab via tag input field.
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Tags field settings"
![](/acf/tags-field-settings.png)
:::
::: tab "Tags field editor"
![](/acf/tag-field-editor.png)
:::
:::: 