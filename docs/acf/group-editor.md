---
metaTitle: ACF - Editor tab - Group of field editor
---

# ACF - Editor tab - Group of field editor

There will be 2 types of group: [unrepeatable & repeatable](/acf/unrepeatable-repeatable.html). In the scope of this document, we only cover repeatable group since it is more complicated than the other one.

::: warning Note:
Value created by fields in a repeatable group is always a **JSON array** and can be retrived on Shopify theme via Liquid iteration tags like `{% for %}` loop. For example:
	
``` liquid
{% for value in product.metafields.namespace.key %}
	{{value}}
{% endfor %}
```
:::

A repeatable group will have a group of buttons:

![](/acf/repeat-group-button.png) 

1. Counter: to indicate the index of each repeated group and total number of a repeatable group.
2. Up - Down buttons: to move a repeated group up or down.
	- Up button does not work if current repeated group is at index 1.
	![](/acf/group-cant-move-up.gif) 

	- Down button does not work if the current repeated group's index is equal to the total number of that repeatable group.
	![](/acf/group-cant-move-down.gif) 

	- Both up and down buttons do not work if the current repeated group's index is equal to 1.
	![](/acf/group-cant-up-down.gif) 

3. Remove button: to remove a repeated group. This do not work if the current repeated group's index is equal to 1.
![](/acf/group-cant-remove.gif) 

4. Repeat button: to repeat a repeatable group