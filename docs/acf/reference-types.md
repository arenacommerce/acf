---
metaTitle: ACF - Reference Types
---
# ACF - Reference Types

Reference types help easily build cross-reference between Shopify objects by allowing users to select one or multiple handles based on type of reference field.

There are 6 reference types in ACF. All value produced by ACF is either in string (non-repeatable field) or JSON string (repeatable field).

![](/acf/reference-types.png) 

- When attribute "**Allow selection of multiple values**" is checked, each value will be separated by **|**, for example: hello<b>|</b>world<b>|</b>2020, book<b>|</b>shoes<b>|</b>pen<b>|</b>eraser.
- **Product**, **Collection** fields can be edited in Editor tab via Shopify resource picker.
<!-- <div class="custom-gallery">
	<div class="img-group">
		![](/acf/collection-field-settings.png) 
		<p>Product - Collection field settings</p>
	</div>
	<div class="img-group">
		![](/acf/resource-picker.png) 
		<p>Product - Collection resource picker</p>
	</div>	
</div> -->
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Product - Collection field settings"
![](/acf/collection-field-settings.png)
:::
::: tab "Product - Collection field editor"
![](/acf/resource-picker.png)
:::
::::

- **Page**, **Blog**, **Article**, **Linklist** fields can be edited in Editor tab via selection field editor.
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Page - Blog - Article - Linklist field settings"
![](/acf/page-field-settings.png)
:::
::: tab "Page - Blog - Article - Linklist field editor"
![](/acf/page-blog-posts-linklist.png)
:::
::::

- In case you don't see item you want to select, press "**load more...**" button.
![](/acf/load-more.png)  
	

