---
metaTitle: ACF - Editor tab
---
# ACF - Editor tab

## How to use field editor to create/edit metafield(s)

**<u>Step 1:</u>** At Editor tab, pick resource you want.

![](/acf/editor-tab.png) 

**<u>Step 2:</u>** According to the resource you pick, it will show a corresponding resource list. Pick an item and you will be taken to its page for creating/editing metafield(s).

![](/acf/resource-list.png) 

You can use the search bar to search for a or many specific items. Search bar works accordingly to selected resource, so please refer to [how to use search bar in Editor tab](/acf/search.html) 

**<u>Step 3:</u>** On item page, a list of field editors according to fields you created on [Field tab](/acf/fields-tab.html#how-to-add-a-field-or-a-group-of-fields)  will show up.

![](/acf/item-editor-page.png) 

- For item page's header, please refer to [function buttons](#function-buttons). 
- A layout of field editors comprises 2 positions, **body** and **sidebar**. Sidebar position is only for group of fields.
- A layout of field editors also consists of 2 types, [field(s)](/acf/field-editor.html) and [group(s) of field(s)](/acf/group-editor.html) 

**<u>Step 4:</u>** Once you have finished creating/editing value for metafield(s), you can click **Save** to update corresponding metafield(s) on Shopify or **Clear** to cancel all changes. 

![](/acf/done-editing.png) 

::: warning Note:
It will take 30 seconds for metafields to take effect on the storefront after successful save.
:::

::: tip
Use `Ctrl + S` (on Windows) or `Cmd + S` (on Mac) to quickly update corresponding metafield(s) on Shopify.
:::

## Function buttons

An item page's header contains the following function buttons:

1. Left side:
	- Back-to-resource-list button
	![](/acf/back-to-resource-list-button.png) 

2. Right side:
	- Left - right arrow buttons: to go to previous or next items.
	- Sync: if by any means your metafield value on Shopify has any changes and is different from its value stored on ACF app, this button can be used to update metafield value on ACF app to match its value on Shopify.

	Please note that **Sync** button only updates fields existing on ACF database.
	- Preview: to view current item page on your store site.
	- More actions:
		![](/acf/more-actions.png) 
		- Edit Fields: to go to corresponding field page on [Field tab](/acf/fields-tab.html#how-to-add-a-field-or-a-group-of-fields) to edit fields or groups.
		- Go to Admin: to go to corresponding Shopify admin page of current item. For example, if current view is product item A, by clicking this button you will be redirected to product item A's Shopify admin page, where you can [edit product item A](https://help.shopify.com/en/manual/products/add-update-products#edit-a-product).




