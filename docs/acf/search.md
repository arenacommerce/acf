---
metaTitle: ACF - How to use search bar in Editor tab
---
# ACF - How to use search bar in Editor tab

ACF's search mechanism is based on Shopify's [REST API](https://help.shopify.com/en/api/reference) and [GraphQL API](https://help.shopify.com/en/api/graphql-admin-api). Due to these APIs' limitation, searching is different according to type of resource; therefore in order for search bar to work well, it is important to know how to use correct keyword for each resource.

## Pages
- Page data only provides search mechanism based on **page title**. Please type exactly page title for the best results. Ex: "about us", "compare page"...

## Blogs
- Blog data only provides search mechanism based on **blog handle**. Please type exactly blog handle for the best results. Ex: "accessories-1", "boots"...

## Posts
- Post data only provides search mechanism based on **blog handle** that they belong to. Please type exactly blog handle for the best results. Ex: "accessories-1", "boots"...

## Collections - Smart collections
- Collection and smart collection data provide search mechanism based on **collection title**. Please type a keyword or sub keyword of collection title. Ex: "laptops", "computer", "laptops & computer"...

## Products
- Product data provides search mechanism based on **product title**, **product type** or **product vendor**. Please type a keyword or sub keyword of product title, product type or product vendor to search results. Ex: "coneco", "fashion", "Armani"...

## Product variants
- Variant data provides search mechanism based on **product title, product type, product vendor** that they belong to. Please type a keyword or sub keyword of product title, product type or vendor to search results. Ex: "coneco", "fashion", "Armani"...

## Customers
- Customer data provides search mechanism based on **customer email** or **customer country**. Please type a keyword or sub keyword of customer email or country to search results. Ex: "john@gmail.com", "United States"

## Orders
- Order data provides search mechanism based on **customer email**. Please type a keyword or sub keyword of email to search results. Ex: "john@gmail.com"

## Draft orders
- Draft order data provides search mechanism based on **customer id** or **order status**. Please type an exactly order status or customer id to search results. Ex: "open", "1546075045934"
