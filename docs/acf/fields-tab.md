---
metaTitle: ACF - Fields tab
---
# ACF - Fields tab

## How to add a field or a group of fields

**<u>Step 1:</u>** At Field tab, pick resource you want to add fields to 

![](/acf/pick_a_resource.png) 

**<u>Step 2:</u>** After picking a resource, you will be taken to a corresponding field page where you can **(a)** create new groups or **(b)** create new fields

( a ) To create new group, press **Add Group Field** button (refer to [Add new group](#add-new-group))

![](/acf/add_group_field_btn.png)  

( b ) To create new field, drag a field from right column and drop it inside left area (refer to [Add new field](#add-new-field))

![](/acf/drag_to_create_new_field.gif)

( c ) To create new field in a group, drag a field from right column and drop it inside a group (refer to [Add new field](#add-new-field))

![](/acf/drag_field_to_group.gif)

( d ) To re-order a group or field, drag and drop it to new postion

![](/acf/sort_group_field.gif) 


**<u>Step 3:</u>** Finally, always remember to click **Save** button to save all changes. In case you want to call off all changes, click **Cancel**

![](/acf/save_cancel_field_page.png) 

::: tip
Use `Ctrl + S` (on Windows) or `Cmd + S` (on Mac) to quickly save all changes.
:::

<!-- ##### <u>Note:</u> There is sample data only for resource "*Products*" due to the fact that metafields are mostly applied to it. --> 
<!-- ![](/acf/product-sample-data.png)  -->

## Add new group

On **Add Group Field** modal, there are following attributes:

![](/acf/add-group-modal.png) 

- **Hide Group Field:** to set whether this group and all fields inside it will be hidden.
- **Repeatable Group:** to set whether this group and all fields inside it will be repeatable. 
- **Collapse Group Field:** to set whether this group will be initially closed when editing.
- **Title:** to set group title.
- **Description:** to set group description.
- **Color:** to set group color, making it easier to differentiate group from group.
- **Position:** to set whether this group will be placed on body or sidebar.
- **Field applies to:** to select the context(s) for which you would like this group and all fields inside it to be editable. By default, groups and fields are editable for all contexts. This attribute only appears in resources:  Products, Product Variants, Collections, Pages, Blogs and Posts.

Please note that there is no need to set up all attributes. Group can be simply added when clicking **Done** button.

![](/acf/done_add_group.png)  

## Add new field

On **Add Field** modal, there are following general attributes:

![](/acf/add_new_field.png) 

- **Hide Field:** to set whether this field will be hidden.
- **Repeatable Field:** to set whether this fields will be repeatable. 
- **Namespace:** This is metafield namespace (A category or container that differentiates your metadata from other metafields.). Default is `arena`. *Please note that this attribute will become uneditable once field has value*.
- **Key:** Required. This is metafield key (The name of the metafield.). *Please note that this attribute will become uneditable once field has value*. 
- **Label:** Required. This is field label.
- **Instruction:** to set field instruction.
- **Field applies to:** to select the context(s) for which you would like this field to be editable. By default, fields are editable for all contexts. This attribute only appears in resources:  Products, Product Variants, Collections, Pages, Blogs and Posts.
- **Allow selection of multiple values:** to set whether this field allows selection of multiple values. This attribute is only present on some of the types of fields.

Based on type of field, there will be additional attributes:

1. <u>Text field</u>:

	- **Text Field Size:** to select the number of text lines to be visible when editing.
	
2. <u>Selection field</u>:
	- **Add values for selection**

3. <u>Choice field</u>:
	- **Use icon?**: to set whether choices will be displayed as icons. There are 2 things to notice if this option is checked:
		1. Inputted value must be in format: `Choice label::: icon's class name`. Accordingly, **choice label** and **icon's class name** must be seperated by 3 colons `:::`
		
			<u>For example</u>: 

			a.	**Choice label**: Left, **Icon's class name**: fas fa-align-left => **Left::: fas fa-align-left**

			b.	**Choice label**: Center, **Icon's class name**: fas fa-align-center => **Center::: fas fa-align-center**

			c.	**Choice label**: Right, **Icon's class name**: fas fa-align-right => **Right::: fas fa-align-right**

			![](/acf/choice-use-icon.png) 

		2. Choice field only supports [Font Awesome 5's  free icons](https://fontawesome.com/icons). To get icon's class name, please follow instruction: 
		
			a. Access this [link](https://fontawesome.com/icons) and pick a free icon.
			![](/acf/pick-an-icon.png) 

			b. Copy icon's class name.
			![](/acf/get-icon-class-name.png) 

4. <u>Number field</u>:
	- **Value type - number data validation**: to set number's value type to be **string** or **integer**. If the type is **integer**, it will not accept decimal. However, this attribute will not work if **Repeatable Field** attribute is checked.
	
5. <u>Range field</u>:
	- **How many steps for Range Slider:** to set number of steps per slide. Please note that step value must be divisible by Min - Max value.
	- **Min value:** to set range slider's lowest value. Please note that **Min value** must be lower than **Max value**.
	- **Max value:** to set range slider's highest value. Please note that **Max value** must be higher than **Min value**.       

6. <u>Date field</u>:
	- **Date Format:** to set date format. *Please note that this attribute will become uneditable once field has value*. 

## Bulk Editor
ACF supports 3 types of Bulk Editor to edit metafields:

- Field's bulk editor (Open bulk editor): edit multiple items at once for a specific metafield.
![](/acf/open-build-editor.gif) 

- Group's bulk editor (Open group bulk editor): edit multiple items at once for a group of metafields.
![](/acf/open-group-build-editor.gif) 

- Resource's bulk editor (Open resource bulk editor): edit multiple items at once for a whole resource.
![](/acf/open-resource-build-editor.gif) 





