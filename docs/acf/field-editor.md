---
metaTitle: ACF - Editor tab - Field editor
---

# ACF - Editor tab - Field editor

There will be 2 types of field editor: [unrepeatable & repeatable](/acf/unrepeatable-repeatable.html). In the scope of this document, we only cover repeatable field since it is more complicated than the other one.

::: warning Note:
Value created by repeatable fields is always a **JSON array** and can be retrived on Shopify theme via Liquid iteration tags like `{% for %}` loop. For example:

``` liquid
{% for value in product.metafields.namespace.key %}
	{{value}}
{% endfor %}
```
:::

A repeatable field will have a group of buttons:

![](/acf/repeatable-field-button.png) 

1. Counter: to indicate the index of each repeated field and total number of a repeatable field.
2. Up - Down buttons: to move a repeated field up or down.
	- Up button does not work if current repeated field is at index 1.
	![](/acf/field-cant-move-up.gif) 

	- Down button does not work if the current repeated field's index is equal to the total number of that repeatable field.
	![](/acf/field-cant-move-down.gif) 

	- Both up and down buttons do not work if the current repeated field's index is equal to 1.
	![](/acf/field-cant-up-down.gif) 

3. Remove button: to remove a repeated field. This do not work if the current repeated field's index is equal to 1.
![](/acf/field-cant-remove.gif) 

4. Repeat button: to repeat a repeatable field
