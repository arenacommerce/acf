---
metaTitle: ACF - Unrepeatable vs Repeatable
---

# ACF - Unrepeatable vs Repeatable

|                            | Unrepeatable 	                    	 | Repeatable |
| --------                   | -------- 		                    	 | -------- |
| **Definition**             | A single field or group in [Editor tab](/acf/editor-tab.html) | A repeatable field or group allows to create a set of sub fields or sub groups which can be repeated endlessly in [Editor tab](/acf/editor-tab.html) |
| **Field:Value Relation**   | 1:1 (One field only has **1** value) 	 | 1:n (One field can have **n** value, presented in form of an array) |
| **Metafield Value Type**   | string, integer		                	 | JSON string |
| **Sample Value**			 | value-1 (string) </br>10 (integer)   	 | ["value-1", "value-2", "value-3"] |
| **Sample Liquid Usage**    | ![](/acf/sample-liquid-usage-2.png)  	 | ![](/acf/sample-liquid-usage-1.png) |


