---
metaTitle: ACF - Installation
---

# App Installation

::: warning <h3><i class="fas fa-exclamation-circle"></i> Notes before Installing Apps</h3>
Before installing, we recommend creating a backup copy of the theme. Creating a backup allows you to quickly and easily return the theme to a previous point in time.

Visit: [Duplicating Themes](https://help.shopify.com/en/manual/using-themes/managing-themes/duplicating-themes) for detailed instructions.
:::

</br>

---

</br>

**Step 1**: Go to the [App Store](https://apps.shopify.com/), and choose the app you’d like to install.

**Step 2**: Click the **Add App** button.

**Step 3**: Log into the App Store.

**Step 4**: Confirm App installation by click **Install App**.

**Step 5**: Use the app! Your newly installed app will live in the App section of your Shopify Admin.


## App Uninstallation

We are very happy if our app can contribute something to your store development. Having said that, we are very sorry to know you want to uninstall it. We hope the following frequently asked questions may help you.

### **1. Confused with something in the app?**

Some of the common reasons a store owner removes an app are:

- they have a question,
- they are confused by the instructions,
- they are nearing the end of the app trial; or
- they are having trouble using the app.

We're here to help!

Please do not hesitate to submit a support request via our [Support Portal](https://arenacommerce.freshdesk.com/support/tickets/new); or send email to support@arenacommerce.com .

### **2. How do I remove the app?**

We understand ACF may not be the perfect fit for every store. If you have decided that our app is not for you and are sure you want to uninstall, follow these instructions: 

- From "Shopify Admin", select **Apps**.
![](/acf/uninstall-1.png)

- Locate **the App** you want to uninstall (Advanced Custom Field)
![](/acf/uninstall-2.png)

- Select the **Remove** button.
![](/acf/uninstall-3.png)

- Please tell us your **FEEDBACK** (Optional)
![](/acf/uninstall-4.png)

- Finally, click **Delete** button.
![](/acf/uninstall-5.png)

### **3. How do I stop being charged for the app?**

When removed from the store, access to the app and monthly billing stops immediately.

### **4. Will all metafields created by ACF also be removed?**

No, uninstalling ACF does not remove metafields created by ACF itself. 

### **5. Liquid code in Usage Examples**

If you followed our usage examples and applied any of them to your theme files, you will have to manually remove any additional liquid code you added to your theme files. 

Please note that ACF is not authorized to make any changes to your theme source code, its only purpose is to create metafields, which are stored on Shopify server.









