---
metaTitle: Adding Custom Fields Data (Metafield Data) to the Cart Page
sidebarDepth: 2
---

This is advanced guide, so please make sure you have understand our app well before going forward. If you are still not sure or have not gone through ACF's basic knowledge, we recommend you go to [Advanced Custom Field](/acf/).

Our examples were created on Shopify's [Debut theme](https://themes.shopify.com/themes/debut/styles/default). This example requires to edit theme code, so users need to have some coding knowledge. Different theme may also has different source code but overall the theme structure should be the same since they are all Shopify themes.

In case you have problem applying any examples here to your theme, please submit a support request via our [Support Portal](https://arenacommerce.freshdesk.com/support/tickets/new); or send email to support@arenacommerce.com.

---

You can easily show field data from products in your cart. This is useful if you want to show information at the last minute about that product. In this example, let say you offer gift wrapping service for your product and you want to show wrapping paper's color as product's additional information. So we will show you how to print information about wrapping paper's color on Cart Page.

- <u>**Step 1**</u>: At Fields tab, choose resource **Products** and create a [Color field](/acf/data-types.html#_10-color).
![](/acf/color-1.png)
- <u>**Step 2**</u>: Click **Save** button to save your changes; then go to Editor tab's resource **Products** and pick a product to start with.
- <u>**Step 3**</u>: Pick a color and click **Save** button to save your changes.
- <u>**Step 4**</u>: Go to [Edit code](https://help.shopify.com/en/manual/using-themes/change-the-layout/theme-code), open file **cart-template.liquid**. In that file, search keyword `</ul>`, then add the following code:

``` liquid
{%- if item.product.metafields.namespace.key != blank -%}
  <li class="product-details__item product-details__item--wp-color">Wrapping paper's color: <span title="{{ item.product.metafields.namespace.key }}" style="height: 20px; width: 20px; display: inline-flex; vertical-align: bottom; background-color:{{ item.product.metafields.namespace.key }}"></span></li>
{%- endif -%}
```

![](/acf/color-2.png) 
- <u>**Step 5**</u>: Click **Save** button to save your changes.

If the above steps are done correctly, the result should be displayed on cart page.
![](/acf/color-3.png) 