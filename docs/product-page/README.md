---
metaTitle: Adding Custom Fields Data (Metafield Data) to the Product Page
sidebarDepth: 2
---

# Adding Custom Fields Data (Metafield Data) to the Product Page

This is advanced guide, so please make sure you have understand our app well before going forward. If you are still not sure or have not gone through ACF's basic knowledge, we recommend you go to [Advanced Custom Field](/acf/).

Our examples were created on Shopify's [Debut theme](https://themes.shopify.com/themes/debut/styles/default). These examples require to edit theme code, so users need to have some coding knowledge. Different theme may also has different source code but overall the theme structure should be the same since they are all Shopify themes.

In case you have problem applying any examples here to your theme, please submit a support request via our [Support Portal](https://arenacommerce.freshdesk.com/support/tickets/new); or send email to support@arenacommerce.com.


## :black_medium_small_square: Displaying Custom Fields as Bullet Points on Product Pages
In this advanced guide, we'll show you how to use ACF to create a bullet list displayed to customers on products. This is another example to show you how you can utilize ACF.

- <u>**Step 1**</u>: At Fields tab, choose resource **Products** and create a [HTML field](/acf/data-types.html#_2-html).
![](/acf/bullet_points_1.png) 

- <u>**Step 2**</u>: Click **Save** button to save your changes; then go to Editor tab's resource **Products** and pick a product to start with.
- <u>**Step 3**</u>: On next page, click **Unordered List** and add content you want.
![](/acf/bullet_points_2.png) 
- <u>**Step 4**</u>: Click **Save** button to save your changes.
- <u>**Step 5**</u>: Go to [Edit code](https://help.shopify.com/en/manual/using-themes/change-the-layout/theme-code), open file **product-template.liquid**. In that file, search keyword **product__price**, then add the following code:

``` liquid
{% if product.metafields.namespace.key != blank %}
  <div class="metafield-bullet-points">{{product.metafields.namespace.key}}</div>
  <style>
    .metafield-bullet-points {
        margin-top: 1rem;
        margin-left: 20px;
        margin-right: 20px;
    }
    .metafield-bullet-points ul li {list-style: initial;}
  </style>
{% endif %}
```
![](/acf/bullet_points_3.png) 
- <u>**Step 6**</u>: Click **Save** button to save your changes.

If the above steps are done correctly, the [result](https://i.arenacommerce.com/go/acf/advancedcustomfield-consectetur-nibh-eget/) should be displayed on corresponding product page.
![](/acf/bullet_points_4.png)

::: tip
Click **Preview** button to quickly jump to corresponding product page.
![](/acf/preview_button.png) 
:::


## :black_medium_small_square: Creating a Simple Shipping Policy on Product Pages
In this advanced tutorial, we will guide you through how to create a product field to display as a per-product shipping policy for your customers. This can be useful if you have different shipping times for different types of products.

In this example, we will show you how to achive this:
- <u>**Step 1**</u>: At Fields tab, choose resource **Products** and create a [Selection field](/acf/data-types.html#_3-selection).
![](/acf/shipping-policy-1.png)
- <u>**Step 2**</u>: Click **Save** button to save your changes; then go to Editor tab's resource **Products** and pick a product to start with.
- <u>**Step 3**</u>: Pick an shipping policy option for your product variant. When everything is fine, click **Save** button to save your changes.
![](/acf/shipping-policy-2.png)
- <u>**Step 4**</u>: Go to [Edit code](https://help.shopify.com/en/manual/using-themes/change-the-layout/theme-code), open file **product-template.liquid**. In that file, search keyword **product-single__description**, then add the following code:

``` liquid
{% if product.metafields.namespace.key != blank %}
  <div class="shipping-policy">
    <h2>Shipping Policy</h2>
    <p>This item takes {{product.metafields.namespace.key}} to ship.</p>
  </div>
{% endif %}
```
![](/acf/shipping-policy-3.png)
- <u>**Step 5**</u>: Click **Save** button to save your changes.

If the above steps are done correctly, the [result](https://i.arenacommerce.com/go/acf/advancedcustomfield-black-fashion-zapda-shoes/) should be displayed on corresponding product page.
![](/acf/shipping-policy-4.png)


## :black_medium_small_square: Displaying Text Based on a Checkbox Value on Product Pages
Theme designers can completely control the output and placement of any custom field in the app using some advanced techniques.

Some possible uses include creating a banner image on every product page, creating a header message area, showing different icons based on product attributes, and many other custom uses.

In this example, we'll add a checkbox to our products that will show one message if checked and another if it's not checked, but you can adapt this example for many other uses.
- <u>**Step 1**</u>: At Fields tab, choose resource **Products** and create a [Checkbox field](/acf/data-types.html#_5-checkbox).
![](/acf/checkbox-1.png)
- <u>**Step 2**</u>: Click **Save** button to save your changes; then go to Editor tab's resource **Products** and pick a product to start with.
- <u>**Step 3**</u>: Check the **Recyclable?** and click **Save** to save your changes.
![](/acf/checkbox-2.png)
- <u>**Step 4**</u>: Go to [Edit code](https://help.shopify.com/en/manual/using-themes/change-the-layout/theme-code), open file **product-template.liquid**. In that file, search keyword **product__price**, then add the following code:

``` liquid
{% if product.metafields.namespace.key == 'true' %}
  <p>This product is recyclable</p>
  {% else %}
  <p>This product is not recyclable</p>
{% endif %}
```
![](/acf/checkbox-3.png)
- <u>**Step 5**</u>: Click **Save** button to save your changes.

If the above steps are done correctly, the [result](https://i.arenacommerce.com/go/acf/advancedcustomfield-coneco-product-sample/) should be displayed on corresponding product page.
![](/acf/checkbox-4.png)


<!-- ## :black_medium_small_square: Displaying An Image Size Chart on Product Pages
In this advanced tutorial, we will give you a glimpse of how to use ACF to add image as custom data to your site. Specifically, in this example we will display an image size chart on product pages. 

- <u>**Step 1**</u>: At Fields tab, choose resource **Products** and create an [Image/File field](/acf/data-types.html#_11-image-file).
![](/acf/image-size-chart-1.png) 
- <u>**Step 2**</u>: Click **Save** button to save your changes; then go to Editor tab's resource **Products** and pick a product to start with.
- <u>**Step 3**</u>: Add your image link and click **Save** to save your changes.
![](/acf/image-size-chart-5.png) 
- <u>**Step 4**</u>: Go to [Edit code](https://help.shopify.com/en/manual/using-themes/change-the-layout/theme-code), open **Snippets** folder then click **Add a new snippet** and name it **customfield-item-snippet**.
![](/acf/image-size-chart-2.png) 
- <u>**Step 5**</u>: Go to this [link](https://github.com/hauph/advanced-custom-field/blob/master/customfield-item-snippet.liquid), copy the file content there and paste it to the newly created file at **Step 4**.
- <u>**Step 6**</u>: Go to **Edit code**, open file **layout/theme.liquid** and add the following code at right before the closing `</head>` tag:

`{%- render 'customfield-item-snippet', type: "style" -%}`

![](/acf/image-size-chart-7.png) 

- <u>**Step 7**</u>: Continue with file **layout/theme.liquid**, add the following code at right before the closing `</body>` tag:

`{%- render 'customfield-item-snippet', type: "initialize" -%}` 

![](/acf/image-size-chart-3.png) 

- <u>**Step 8**</u>: Go to **Edit code**, open file **sections/product-template.liquid** then search for keyword `{% endform %}` and paste the following code right below it:

`{%- render 'customfield-item-snippet', type: "image_size_chart", object: product -%}`

![](/acf/image-size-chart-4.png) 

If the above steps are done correctly, the [result](https://i.arenacommerce.com/go/acf/advancedcustomfield-daltex-product-example/) should be displayed on corresponding product page.

:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Modal close"
![](/acf/image-size-chart-6.png) 
:::
::: tab "Modal open"
![](/acf/image-size-chart-8.png) 
:::
:::: -->


## :black_medium_small_square: Displaying Product Tabs on Product Pages
In this advanced guide, we’ll show you how to use ACF to create a product tabs. 

- <u>**Step 1**</u>: At Fields tab, choose resource **Products** and [create a new group](/acf/fields-tab.html#add-new-group).
![](/acf/product-tabs-group.png) 
- <u>**Step 2**</u>: In newly created group, add a [Text field](/acf/data-types.html#_1-text)
![](/acf/product-tabs-text-field.png) 
- <u>**Step 3**</u>: Right below newly create Text field, add an [HTML field](/acf/data-types.html#_2-html)
![](/acf/product-tabs-html-field.png) 
- <u>**Step 4**</u>: Click **Save** button to save your changes; then go to Editor tab's resource **Products** and pick a product to start with.
- <u>**Step 5**</u>: Add tab title and tab content. Repeat **Product Tabs** group to create more tab title and tab content. Always remember to click **Save** to save your changes.
![](/acf/product-tabs-editor.png) 
- <u>**Step 6**</u>: Download these 2 files [vanilla-js-tabs.min.js](https://raw.githubusercontent.com/zoltantothcom/vanilla-js-tabs/master/dist/vanilla-js-tabs.min.js) and [vanilla-js-tabs.css](https://raw.githubusercontent.com/zoltantothcom/vanilla-js-tabs/master/dist/vanilla-js-tabs.css). 
- <u>**Step 7**</u>: Go to [Edit code](https://help.shopify.com/en/manual/using-themes/change-the-layout/theme-code), open **Assets** folder and upload 2 newly downloaded files at step 6.
- <u>**Step 8**</u>: Open **Snippets** folder then click **Add a new snippet** and name it **arn_product_tabs**. Next, copy the following lines of code to the newly created file
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Code"
``` liquid
{% assign prodMf = product.metafields %}
{% assign tab_titles = prodMf.arena.tab_title %}
{% assign tab_contents = prodMf.arena.tab_content %}

{% if tab_titles.size > 0 and tab_contents.size > 0 %}
  {{ 'vanilla-js-tabs.css' | asset_url | stylesheet_tag }}
  <script src="{{ 'vanilla-js-tabs.min.js' | asset_url }}"></script>

  <div class="js-tabs" id="tabs">
      <ul class="js-tabs__header">
          {% for title in tab_titles %}
            <li><a href="#" class="js-tabs__title">{{title}}</a></li>
          {% endfor %}
      </ul>
    
      {% for content in tab_contents %}
        <div class="js-tabs__content">{{content}}</div>
      {% endfor %}
  </div>

  <script>
    var tabs = new Tabs({
        elem: "tabs",
        open: 0
    });
  </script>
{% endif %}
```
:::
::: tab "Demo"
![](/acf/product-tabs-code-3.png) 
:::
::::
- <u>**Step 9**</u>: Open **product-template.liquid** file and add `{% include 'arn_product_tabs' %}` at where you want the product tabs to show up.
![](/acf/product-tabs-code-4.png) 
::: warning Note:
  File **product-template.liquid** contains main parts of a product page. Different themes may have different names for this file, but its purpose should be all the same. 

  In can your source code does not have **product-template.liquid** file, please contact theme author for the main file which makes up the product page.
:::

If the above steps are done correctly, the [result](https://electro-5-demo.myshopify.com/products/5-panel-hat-abc) should be displayed on corresponding product page.
![](/acf/product-tabs-result.png) 









