---
metaTitle: Adding Custom Fields Data (Metafield Data) to the Article Page
sidebarDepth: 2
---

This is advanced guide, so please make sure you have understand our app well before going forward. If you are still not sure or have not gone through ACF's basic knowledge, we recommend you go to [Advanced Custom Field](/acf/).

Our examples were created on Shopify's [Debut theme](https://themes.shopify.com/themes/debut/styles/default). This example requires to edit theme code, so users need to have some coding knowledge. Different theme may also has different source code but overall the theme structure should be the same since they are all Shopify themes.

In case you have problem applying any examples here to your theme, please submit a support request via our [Support Portal](https://arenacommerce.freshdesk.com/support/tickets/new); or send email to support@arenacommerce.com.

---

In this example, we will show you how to add custom field data to articles to pretend their total views.
- <u>**Step 1**</u>: At Fields tab, choose resource **Posts** and create a [Number field](/acf/data-types.html#_7-number).
![](/acf/post-view-1.png) 
- <u>**Step 2**</u>: Click **Save** button to save your changes; then go to Editor tab's resource **Posts** and pick a post to start with.
![](/acf/post-view-2.gif) 
- <u>**Step 3**</u>: Input a number and click **Save** button to save your changes.
![](/acf/post-view-3.png) 
- <u>**Step 4**</u>: Go to [Edit code](https://help.shopify.com/en/manual/using-themes/change-the-layout/theme-code), open file **article-template.liquid**. In that file, search keyword **rte**, then add the following code:

``` liquid
{% if article.metafields.namespace.key != blank %}
  <div class="total-view"><h5>Total view(s): <span style="font-weight: 100;">{{article.metafields.namespace.key}}</span></h5></div>
{% endif %}
```
![](/acf/post-view-4.png) 
- <u>**Step 5**</u>: Click **Save** button to save your changes.

If the above steps are done correctly, the [result](https://i.arenacommerce.com/go/acf/advancedcustomfield-blogs-news-1_sweet-memorires-in-our-store/) should be displayed on corresponding article page.
![](/acf/post-view-5.png) 